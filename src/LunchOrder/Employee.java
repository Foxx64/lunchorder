package LunchOrder;

import java.util.ArrayList;

public class Employee {
	private String name ;
	private int orderPrice;
	ArrayList<Dish> dishes = new ArrayList<>();
	
	public void addDish (Dish dish){
		dishes.add(dish);
	}
	
	public int getOrdersPrice() {
		return orderPrice;
	}

	public void setOrdersPrice(int ordersPrice) {
		this.orderPrice = ordersPrice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLunchesPrice (){
		double sum = 0;
		for (int i = 0; i < dishes.size(); i++){
			sum = sum + dishes.get(i).getPrice();
		}
		return sum;
	}
	
	

}
