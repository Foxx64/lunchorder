package LunchOrder;

import java.util.ArrayList;

public class LunchOrder {
	ArrayList<Employee> employees = new ArrayList<>();
	LunchOrder(){
		Employee pupkin = new Employee();
		pupkin.setName("Пупкин А.");
		Dish salatIsuminka = createDichSalat();
		Dish pechen = createDichPechen();
		Dish ris = createDichRis();
		Dish rulet = createDishRulet();
		pupkin.addDish(rulet);
		pupkin.addDish(pechen);
		pupkin.addDish(salatIsuminka);
		pupkin.addDish(ris);
		employees.add(pupkin);
		
		Employee krivenko = new Employee();
		krivenko.setName("Кривенко C.");
		Dish salatLukoshko = createDichSalatLukoshko();
		Dish supKuriniy = createDichSupKuriniy();
		Dish swinina = createDichSwinina();
		Dish makaroni = createDichMakaroni();
		Dish pirogYablochniy = createDichPirogYablochniy();
		krivenko.addDish(salatLukoshko);
		krivenko.addDish(supKuriniy);
		krivenko.addDish(swinina);
		krivenko.addDish(makaroni);
		krivenko.addDish(pirogYablochniy);
		employees.add(krivenko);
		
		Employee gaychenov = new Employee();
		gaychenov.setName("Гайченов П.");
		Dish supVegetarianskiy = createDichSupVegetarianskiy();
		Dish ribnoeFile = createDichRibnoeFile();
		Dish pirog = createDichPirog();
		gaychenov.addDish(supVegetarianskiy);
		gaychenov.addDish(ribnoeFile);
		gaychenov.addDish(rulet);
		gaychenov.addDish(pirog);
		employees.add(gaychenov);
		
		Employee suhogilov = new Employee();
		suhogilov.setName("Сухожилов Л.");
		suhogilov.addDish(pechen);
		suhogilov.addDish(salatIsuminka);
		suhogilov.addDish(supKuriniy);
		suhogilov.addDish(makaroni);
		suhogilov.addDish(pirog);
		employees.add(suhogilov);
		
		ArrayList<ReportLine> reportLines = new ArrayList<>();
		
		for (int i = 0; i < employees.size(); i++){
			Employee employee = employees.get(i);
			for (int j = 0; j < employee.dishes.size(); j++){
				boolean containDish = false;
				Dish dish = employee.dishes.get(j);
				for (int k = 0; k < reportLines.size();k++){
					if (reportLines.get(k).getName() == dish.getName()){
						int amount = reportLines.get(k).getAmount()+1;
						reportLines.get(k).setAmount(amount);
						double price = reportLines.get(k).getPrice() + dish.getPrice();
						reportLines.get(k).setPrice(price);
						containDish = true;
						break;
					}
				}
				if (!containDish){
					ReportLine reportline = new ReportLine();
					String name = dish.getName();
					reportline.setName(name);
					reportline.setAmount(1);
					double price = dish.getPrice();
					reportline.setPrice(price);
					reportLines.add(reportline);
				}
			}
		}
		printReport(reportLines);
		printLunch(employees);
	}
	
	private void printLunch(ArrayList<Employee> employees) {
		String title = String.format("%-35s %-32s %-30s", "Служащий", "Наименование блюда", "Стоимость обеда, руб.");
		System.out.println(title);
		
		for (int i = 0; i < employees.size(); i++){
			Employee employee = employees.get(i);
			System.out.println(employee.getName() );
			for (int j = 0; j < employee.dishes.size(); j++){
				String dish = String.format(" %-35s %-35s ","",employee.dishes.get(j).getName());
				System.out.println(dish);
			}
			String priceLunch = String.format(""
					+ "%-70s %-35s","", employee.getLunchesPrice());
			System.out.println(priceLunch);
		}
	}

	private void printReport (ArrayList<ReportLine> reportLines){
		String string = String.format("%-35s %-19s %-8s %-1s","Наименование блюда","Количество, шт","","Цена, руб");
		System.out.println(string);
		 
		for (int i = 0; i < reportLines.size(); i++){
			String report = String.format("%-35s      %-25s %-6s",reportLines.get(i).getName(),
					reportLines.get(i).getAmount(),reportLines.get(i).getPrice());
			System.out.println(report);
		}
		String fullPrice = String.format("%-66s %s","Стоимость заказа", fullPrice(reportLines));
		System.out.println(fullPrice);
		System.out.println();
	}
	
	private double fullPrice(ArrayList<ReportLine> reportLines){
		double sum = 0;
		for (int i = 0; i < reportLines.size(); i++) {
			sum = sum + reportLines.get(i).getPrice();
		}
		return sum;
	}

	private Dish createDichPirog() {
		Dish pirog = new Dish();
		pirog.setName("Пирог с повидлом");
		pirog.setWeight(60);
		pirog.setPrice(13);
		return pirog;
	}

	private Dish createDichRibnoeFile() {
		Dish ribnoeFile = new Dish();
		ribnoeFile.setName("Рыбное филе с помидоркой");
		ribnoeFile.setWeight(80);
		ribnoeFile.setPrice(54);
		return ribnoeFile;
	}

	private Dish createDichSupVegetarianskiy() {
		Dish supVegetarianskiy = new Dish();
		supVegetarianskiy.setName("Суп болгарский вегетарианский");
		supVegetarianskiy.setWeight(250);
		supVegetarianskiy.setPrice(21);
		return supVegetarianskiy;
	}

	private Dish createDichPirogYablochniy() {
		Dish pirogYablochniy = new Dish();
		pirogYablochniy.setName("Пирог с яблоками");
		pirogYablochniy.setWeight(60);
		pirogYablochniy.setPrice(13);
		return pirogYablochniy;
	}

	private Dish createDichMakaroni() {
		Dish makaroni = new Dish();
		makaroni.setName("Макароны отварные");
		makaroni.setWeight(170);
		makaroni.setPrice(15);
		return makaroni;
	}

	private Dish createDichSwinina() {
		Dish swinina = new Dish();
		swinina.setName("Свинина запеченная По - гусарски");
		swinina.setWeight(80);
		swinina.setPrice(68);
		return swinina;
	}

	private Dish createDichSupKuriniy() {
		Dish supKuriniy = new Dish();
		supKuriniy.setName("Суп куриный с грибами");
		supKuriniy.setWeight(250);
		supKuriniy.setPrice(34);
		return supKuriniy;
	}

	private Dish createDichSalatLukoshko() {
		Dish salatLukoshko = new Dish();
		salatLukoshko.setName("Салат Лукошко");
		salatLukoshko.setWeight(100);
		salatLukoshko.setPrice(40);
		return salatLukoshko;
	}

	private Dish createDichRis() {
		Dish ris = new Dish();
		ris.setName("Рис отварной с маслом");
		ris.setWeight(170);
		ris.setPrice(16);
		return ris;
	}

	private Dish createDichSalat() {
		Dish salat = new Dish();
		salat.setName("Салат Изюминка");
		salat.setWeight(100);
		salat.setPrice(35);
		return salat;
	}

	private Dish createDichPechen() {
		Dish pechen = new Dish();
		pechen.setName("Печень по королевски");
		pechen.setWeight(150);
		pechen.setPrice(54);
		return pechen;
	}

	private Dish createDishRulet() {
		Dish rulet = new Dish();
		rulet.setName("Рулет с изюмом");
		rulet.setWeight(75);
		rulet.setPrice(15);
		return rulet;
	}

	public static void main(String[] Args){
		new LunchOrder();
	}
}
